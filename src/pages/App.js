import MyInfo from "../components/MyInfo";
import { Router, Link } from "@reach/router";
import React from "react";
import Layout from "../components/Layout";
const App = ({ location }) => {
  return (
    <Layout>
      <nav>
        <Link to="/app/info">Info</Link>
      </nav>
      <Router basepath="/app">
        <MyInfo path="/info" />
      </Router>
      <h1>
        Welcome to the App Page <span>{location.pathname}</span>
      </h1>
    </Layout>
  );
};

export default App;
